from random import randint

players = []
sushi = {}
possible = []
value = {}


def __init__(number_of_players):
    global sushi
    global possible
    global value

    sushi = {'tempura':     14,
             'sashimi':     14,
             'dumplings':   14,
             '2_maki_roll': 12,
             '3_maki_roll': 8,
             '1_maki_roll': 6,
             'salmon':      10,
             'squid':       5,
             'egg':         5,
             'pudding':     10,
             'wasabi':      6,
             'chopsticks':  4}

    possible = ['tempura',
                'sashimi',
                'dumplings',
                '2_maki_roll',
                '3_maki_roll',
                '1_maki_roll',
                'salmon',
                'squid',
                'egg',
                'pudding',
                'wasabi',
                'chopsticks']

    default_value()

    deal(number_of_players)


def default_value():
    global value

    value = {'tempura': 2.5,
             'sashimi': 3.33,
             'dumplings': 2,
             '2_maki_roll': 2.25,
             '3_maki_roll': 2.25,
             '1_maki_roll': 2.25,
             'salmon': 2,
             'squid': 3,
             'egg': 1,
             'pudding': 6,
             'wasabi': 3,
             'chopsticks': 0.5}


# nop == Number of Players
def deal(nop):
    global players
    global sushi
    global possible

    players = [None] * nop

    cards = 0
    if nop == 2:
        cards = 10
    elif nop == 3:
        cards = 9
    elif nop == 4:
        cards = 8
    elif nop == 5:
        cards = 7

    for i in range(0, nop):
        players[i] = []
        for _ in range(0, cards):
            random = possible[randint(0, len(possible) - 1)]
            players[i].append(random)
            if sushi[random]:
                sushi[random] = sushi[random] - 1
            if sushi[random] < 1:
                del sushi[random]
                del possible[possible.index(random)]


def swap():
    global players
    buffer = players[0]
    for i in range(0, len(players)):
        if i + 1 < len(players):
            players[i] = players[i + 1]
    players[len(players) - 1] = buffer


def play():
    global players
    global value
    potential_value = 0
    played_card = ''
    played_first = [False] * len(players)
    known_cards = []

    while len(players[0]) > 0:
        player = 1
        for i in players:
            for j in i:
                if value[j] > potential_value:
                    potential_value = value[j]
                    if not ai(played_card, known_cards, i):
                        continue
                    else:
                        played_card = j
                if played_first:
                    known_cards.append(j)
            print("PLAYER " + str(player) + " PLAYED: " + played_card)
            played_first[players.index(i)] = True
            del i[i.index(played_card)]
            potential_value = 0
            played_card = ''
            player = player + 1
        swap()
        known_cards = []
    score()


def ai(card, known_cards, player):
    global value

    if value['salmon'] > 2 or value['squid'] > 3 or value['egg'] > 1:
        if card == 'salon' or card == 'squid' or card == 'egg':
            default_value()
            return True
        else:
            return False

    if card == 'wasabi':
        if 'salmon' in player and player.count('salmon') > 2 or 'squid' in player and player.count('squid') > 2 or 'egg' in player and player.count('egg') > 2:
            value['salmon'] = 12
            value['squid'] = 18
            value['egg'] = 6
            return True
        elif 'salmon' in known_cards or 'squid' in known_cards or 'egg' in known_cards:
            count = known_cards.count('salmon') + known_cards.count('squid') + known_cards.count('egg')
            if count > 3:
                value['salmon'] = 6
                value['squid'] = 9
                value['egg'] = 3
                return True
            else:
                default_value()
                return False
        else:
            default_value()
            return False
    elif card == 'tempura':
        return False
    elif card == 'sashimi':
        return False
    return True


def score():
    return 1


def main():
    __init__(2)
    play()
    return

main()




