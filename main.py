import sys
from random import randint


checkpoint = 0
health = 20
shield = 0
default_shield = 0
default_health = 20
combat_strength = 6


def success(direction, success_eval, damage_points):
    global checkpoint

    random = randint(0, len(success_eval) - 1)

    if direction in success_eval:
        if direction == success_eval[random]:
            checkpoint += 1
            return
        else:
            damage(damage_points)
            return
    else:
        print('TRY AGAIN')


def pick_up(pick, strength, combat_percentage):
    global shield
    global default_shield

    if pick == 'yes' or pick == 'ye' or pick == 'y':
        print('You picked up the shield.')
        default_shield = strength
        shield = strength
    else:
        return
    random = randint(0, 100)
    if random <= combat_percentage:
        combat()


def combat():
    enemies = []
    for i in range(0, randint(1, 10)):
        enemies.append(randint(1, 20))
    print("You were ambushed by", len(enemies), "enemies.")
    while len(enemies) != 0:
        for i in enemies:
            input_dir = input("Do you wish to try and escape? YES/NO\n")
            input_dir = input_dir.lower()
            if input_dir == 'yes' or input_dir == 'ye' or input_dir == 'y':
                if randint(0, 100) > 30:
                    print('Escaped successfully!')
                    return
                else:
                    print("You couldn't escape...")
            if i > combat_strength * (randint(0, 100) / 33.33):
                damage(i)
                i -= combat_strength
            else:
                print("You defeated an enemy")
                del enemies[enemies.index(i)]


def damage(damage_points):
    global health
    global shield

    if shield > 0:
        shield -= damage_points
        print('SHIELD ABSORBED THE BLOW!')
        if shield <= 0:
            print('YOUR SHIELD BROKE!')
        return

    health -= damage_points
    if health <= 0:
        die()
        return
    print('YOU TOOK DAMAGE!\nREMAINING HEALTH:', health)


def die():
    global health
    global shield

    did_die = input('YOU DIED\nTry again? YES/NO\n')
    did_die = did_die.lower()
    if did_die == 'yes' or did_die == 'y' or did_die == 'ye':
        health = default_health
        shield = default_shield
        return
    else:
        sys.exit(1)


def main():
    global checkpoint
    global health
    global shield
    global default_health
    global default_shield

    while True:
        if checkpoint == 0:
            input_dir = input("There's a monster chasing you. Which way do you go? LEFT/RIGHT\n")
            input_dir = input_dir.lower()
            success(input_dir, ['left', 'right'], 30)
        elif checkpoint == 1:
            pick = input("You've escaped the monster. You looked around yourself and there's a shield.\nDo you pick "
                         "it up? YES/NO\n")
            pick = pick.lower()
            pick_up(pick, 10, 50)
            checkpoint += 1
        elif checkpoint == 2:
            input_dir = input("All of a sudden monsters start appearing. You have to run where do you go? LEFT/RIGHT\n")
            input_dir = input_dir.lower()
            success(input_dir, ['left', 'right'], 30)
        elif checkpoint == 3:
            print('CONGRATULATIONS, YOU WON!')
            checkpoint += 1
            continue
        else:
            break

main()
